#!/bin/bash

# LIBVERSION shall only include MAJOR.MINOR.PATCH for require
LIBVERSION=$(echo ${PKG_VERSION}| cut -d'.' -f1-3)

# Clean between variants builds
make -f Makefile.E3 clean

make -f Makefile.E3 MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION}
make -f Makefile.E3 MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} db
make -f Makefile.E3 MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} install

